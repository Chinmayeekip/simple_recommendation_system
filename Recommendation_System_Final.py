#!/usr/bin/env python
# coding: utf-8

# In[27]:


import pandas as pd
from tkinter import *
from tkinter import ttk
from scipy.sparse import csr_matrix
from sklearn.neighbors import NearestNeighbors

#pip3 install fuzzywuzzy for package
from fuzzywuzzy import process
import random

movies = 'movies.csv'
ratings = 'ratings.csv'
df_movies = pd.read_csv(movies,usecols=['movieId','title'],dtype = {'movieId':'int32','title':'str'})

df_ratings=pd.read_csv(ratings,usecols=['userId','movieId','rating'],dtype = {'userId':'int32','movieId':'int32','rating':'float32'})

movie_user = df_ratings.pivot(index = 'movieId',columns = 'userId',values = 'rating').fillna(0)

mat_movie_user = csr_matrix(movie_user.values)

model_knn = NearestNeighbors(metric = 'cosine',algorithm = 'brute',n_neighbors = 10)

model_knn.fit(mat_movie_user)




def recommend(movie_name,data,model,n_recommend):
    T1.delete('1.0', END)
    

        
    model.fit(data)
    list = []
    idx = process.extractOne(movie_name,df_movies['title'])[2]
    print('Movie Selected: ',df_movies['title'][idx])

    distances,indices=model.kneighbors(data[idx],n_neighbors =n_recommend)
    for i in indices:
        movielist = pd.DataFrame(df_movies['title'][i])
        movielist = movielist.reset_index()
        movielist = movielist.drop('index',axis = 1)

    T1.insert(0.0,movielist)


options =  df_movies['title'].tolist()




root = Tk()
root.geometry("485x300")
root.title('Movie Recommendation System')
root.iconbitmap('logo2.ico')
L1 = Label(root,text='Enter Movie Name:   ')
L2 = Label(root,text = 'Top 10 Movie Recommendation:')

combo = ttk.Combobox(root,value = options,width = 30)
combo.current(random.randint(0, 8000))
combo.bind("<<Comboboxselected>>")
L1.grid(row = 0,column = 0)
combo.grid(row = 0,column = 1)
L2.grid(row = 2,column = 0)
B1 = Button(root,text = 'Search',command = lambda: recommend(combo.get(),mat_movie_user,model_knn,10))
B1.grid(row = 1, column = 1)
B1.grid(row = 1, column = 1)

T1 =Text(root,width=60,height=13, wrap=WORD)
T1.grid(row = 3,columnspan = 2,sticky = W)

root.mainloop()





